<?php

/**
 * @file
 */

/**
 * Simple filter to handle matching of multiple options selectable via checkboxes
 *
 * Definition items:
 * - options callback: The function to call in order to generate the value options. If omitted, the options 'Yes' and 'No' will be used.
 * - options arguments: An array of arguments to pass to the options callback.
 *
 * @ingroup views_filter_handlers
 */
class views_attachment_tabs_handler_filter extends views_handler_filter_in_operator {

  /**
   * @override get_value_options
   * @return array|mixed
   */
  function get_value_options() {
    $attachments = _views_attachment_tabs_get_attachments($this->view);
    foreach ($attachments as $attachment) {
      $this->value_options[$attachment->attachment_id] = $attachment->attachment_title;
    }
    return $this->value_options;
  }


  /**
   * Add this filter to the query.
   *
   * Due to the nature of fapi, the value and the operator have an unintended
   * level of indirection. You will find them in $this->operator
   * and $this->value respectively.
   */
  function query() {
    $this->query->add_filter($this);
    return;
  }


  function option_definition() {
    $options = parent::option_definition();

    $options['operator']['default'] = 'in';
    $options['value']['default'] = array();
    $options['expose']['contains']['reduce'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  /**
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   */
  function operators() {
    $operators = array(
      'in' => array(
        'title' => t('Is one of'),
        'short' => t('in'),
        'short_single' => t('='),
        'method' => 'op_simple',
        'values' => 1,
      ),
    );
    return $operators;
  }

  function __toString() {
    return $this->generate();
  }

  function generate() {
    $operator = $this->options['expose'] ? $this->operator : $this->options['operator'];
    $values = $this->value;
    foreach ($values as $key => $value) {
      $values[$key] = '"' . $value . '"';
    }
    $values = implode(',', $values);

    return $this->options['field'] . ' ' . $operator . ' (' . $values . ')';
  }
}