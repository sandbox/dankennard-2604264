<?php

/**
 * @file
 * Row style plugin for displaying the results as entities.
 */

/**
 * Plugin class for displaying Views results with entity_view.
 */
class views_attachment_tabs_plugin_row_attachment_view extends views_plugin_row {

  protected $entity_type, $entities;

  /**
   * @param $values
   */
  public function pre_render($values) {
  }

  /**
   * @param stdClass $values
   * @return string
   */
  public function render($row) {
    $display_id = $row->attachment_id;
    $view = views_get_view($this->view->name);

    $view->original_args = $this->view->args;
    $args = $this->display->handler->get_option('inherit_arguments') ? $this->view->args : array();

    if (! empty($args)) {
      $view->set_arguments($args);
    }

    $view->set_display($display_id);
    $attachment = $view->execute_display($display_id);

    $view->destroy();
    return $attachment;
  }
}
