<?php

/**
 * @file
 */

/**
 * Style plugin to display Quicktabs.
 */
class views_attachment_tabs_style_plugin extends views_plugin_style {

  // Allow some options for the style.
  function option_definition() {
    $options = parent::option_definition();
    $options['tab_style'] = array('default' => 'default');
    return $options;
  }

  // Create the options form.
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = array();
    $styles = module_invoke_all('quicktabs_tabstyles');
    // The keys used for options must be valid html id-s.
    // Removing the css file path, because that can't be used.
    foreach ($styles as $style) {
      $options[$style] = $style;
    }
    ksort($options);
    $form['tab_style'] = array(
      '#type' => 'select',
      '#title' => t('Tab style'),
      '#options' => array('nostyle' => t('No style'), 'default' => t('Default style')) + $options,
      '#default_value' => $this->options['tab_style'],
      '#description' => t('The tab style that will be applied to this set of tabs. Note that this style may not show in the live preview.'),
      '#weight' => -5,
    );
  }

  // Ensure we have all the settings necessary to render into tabs.
  function validate() {
    $errors = parent::validate();
    return $errors;
  }

  // Override the render functionality.
  function render() {
    if (empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }

    $view = $this->view;
    $qt_name = 'view__' . $view->name .'__'. $view->current_display;

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);
    $tabs = array();

    foreach ($sets as $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$row->attachment_title] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }

      // If not grouped, there's just one set of rows that we loop through.
        foreach ($rows as $row_title => $row) {
          $tabs[] = array(
            'title' => $row_title,
            'contents' => array('#markup' => $row),
          );
        }

    }

    $overrides = array('style' => $view->style_options['tab_style'], 'sorted' => TRUE);
    $quicktabs = quicktabs_build_quicktabs($qt_name, $overrides, $tabs);
    $output = drupal_render($quicktabs);

    // If doing a live preview, add the JavaScript directly to the output.
    if (isset($view->live_preview) && $view->live_preview) {
      $js = drupal_add_js();
      $qtsettings = array();
      foreach ($js['settings']['data'] as $settings) {
        if (isset($settings['quicktabs']['qt_'. $qt_name])) {
          $qtsettings = $settings['quicktabs']['qt_'. $qt_name];
          break;
        }
      }

      $output .= "<script type=\"text/javascript\">\n";
      $output .= "Drupal.settings.quicktabs = Drupal.settings.quicktabs || {};\n";
      $output .= "jQuery.extend(Drupal.settings.quicktabs, ". json_encode(array('qt_'. $qt_name => $qtsettings)) .");\n";
      $output .= "</script>\n";
    }

    unset($view->row_index);

    return $output;
  }
}
