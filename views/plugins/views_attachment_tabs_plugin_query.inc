<?php
/**
 * @file
 */

/**
 * Class views_attachment_tabs_plugin_query
 */
class views_attachment_tabs_plugin_query extends views_plugin_query {

  /**
   * @param bool $get_count
   */
  function query($get_count = FALSE) {
  }

  function add_field($table, $field, $alias = '', $params = array()) {
  }

  function add_orderby($orderby) {
  }

  function add_filter($filter) {
    $this->filter[$filter->options['group']][] = $filter;
  }

  function add_argument($argument) {
  }

  function add_relationship() {
  }

  function build(&$view) {
    $view->build_info['query'] = $this->query();
  }

  function add_tag() {
  }

  function add_table() {
  }

  function placeholder() {
    return;
  }

  /**
   * @param $group
   * @param $field
   * @param null $value
   * @param null $operator
   */
  function add_where($group, $field, $value = NULL, $operator = NULL) {
  }

  function add_where_expression($group, $snippet, $args = array()) {
  }

  /**
   * @param view $view
   */
  function execute(&$view) {
    if (! empty($this->filter[1][0])) {
      $attachments = _views_attachment_tabs_get_attachments($view);
      foreach ($this->filter[1][0]->value as $attachment_id => $attachment) {
        if (in_array($attachment, array_keys($attachments))) {
          $view->result[] = $attachments[$attachment_id];
        }
      }
    }
    else {
      foreach (_views_attachment_tabs_get_attachments($view) as $attachment_id => $attachment) {
        $view->result[] = $attachment;
      }
    }
    return;
  }
}