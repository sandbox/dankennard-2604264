<?php
/**
 * @file
 */

/**
 * @Implements hook_views_plugins().
 */
function views_attachment_tabs_views_plugins() {

  return array(
    'module' => 'views_attachment_tabs',

    // Query plugin definition
    'query' => array(
      'views_attachment_tabs_plugin_query' => array(
        'title' => t('Views Attachment Tabs Query'),
        'help' => t('Views attachment tabs query object.'),
        'handler' => 'views_attachment_tabs_plugin_query',
      ),
    ),

    // Row plugin definition
    'row' => array(
      'views_attachment_tabs' => array(
        'title' => t('Views Attachment Tabs'),
        'help' => t('Displays the views attachment tabs'),
        'handler' => 'views_attachment_tabs_plugin_row_attachment_view',
        'uses fields' => FALSE,
        'uses options' => FALSE,
        'type' => 'normal',
      ),
    ),

    'style' => array(
      'views_attachment_tabs' => array(
        'title' => t('Views Attachment Tabs'),
        'help' => t('Display views attachments in Quicktabs.'),
        'handler' => 'views_attachment_tabs_style_plugin',
        'theme' => 'quicktabs_view',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),

    //
    'display' => array(
      'views_attachment_tabs_view' => array(
        'title' => t('Attachment Tabs'),
        'help' => t("Display views attachments in tabs."),
        'handler' => 'views_attachment_tabs_plugin_attachment',
        'theme' => 'views_view',
        'contextual links locations' => array(),
        'use ajax' => TRUE,
        'use pager' => FALSE,
        'use more' => TRUE,
        'accept attachments' => TRUE,
        'help topic' => 'display-attachment',
        'table' => 'views_attachment_tabs',
      ),
    ),
  );
}
